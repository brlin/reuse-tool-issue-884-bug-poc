# reuse-tool issue 884 bug poc

This project is used to reproduce the [reuse-tool does not properly handle the whitespace control modifiers in the Jinja2 comment lines · Issue #884 · fsfe/reuse-tool](https://github.com/fsfe/reuse-tool/issues/884) issue

<https://gitlab.com/brlin/reuse-tool-issue-884-bug-poc>  
[![The GitLab CI pipeline status badge of the project's `main` branch](https://gitlab.com/brlin/reuse-tool-issue-884-bug-poc/badges/main/pipeline.svg?ignore_skipped=true "Click here to check out the comprehensive status of the GitLab CI pipelines")](https://gitlab.com/brlin/reuse-tool-issue-884-bug-poc/-/pipelines)

## Prerequisite

You need to install the [pre-commit](https://pre-commit.com/) software.

## Usage

Run the following command to reproduce the problem:

```bash
pre-commit run --all-files
```

Edit the [.pre-commit-config.yaml](.pre-commit-config.yaml) file to change the version of reuse-tool used for checking.

## Reference

[reuse-tool does not properly handle the whitespace control modifiers in the Jinja2 comment lines · Issue #884 · fsfe/reuse-tool](https://github.com/fsfe/reuse-tool/issues/884)

## Licensing

CC-BY-SA-4.0+
